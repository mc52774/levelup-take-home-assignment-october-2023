const express = require('express')
const app = express()
const path = require('path');
const bodyParser = require('body-parser');

app.use(express.static(path.join(__dirname, 'Frontend')));
app.use(bodyParser.urlencoded({ extended: false }));
app.set('view engine', 'ejs')

function checkExpiry(date){
    current_date=new Date();
    current_month=current_date.getMonth()+1;
    current_year=current_date.getFullYear();
    input_year=date.substring(0,4)
    input_month=date.substring(5,7)
    if(input_month.startsWith("0")){
        input_month=input_month.substring(1,2)
    }
    if(current_year<input_year){
        return true;
    }
    else if(current_month<input_month){
        return true;
    }
    else{
        return false;
    }
}

function checkCVV(number,cvv){
    if(cvv.length==3){
        return true;
    }
    if(cvv.length==4 && (number.startsWith("34") || number.startsWith("37"))){
        return true;
    }
    return false;
}

function checkPAN(number){
    if(number.length>=16 && number.length<=19){
        return true;
    }
    return false;
}

function checkLUHN(number){
    last_digit=number[number.length-1];
    payload=number.substring(0,number.length-1);
    check_digit=0;
    multiplier=2;
    for(let i=payload.length-1;i>=0;i--){
        sum_number=payload[i]*multiplier;
        if(sum_number>9){
            sum=0;
            while(sum_number>0){
                sum+=sum_number%10;
                sum_number=Math.floor(sum_number/10);
            }
            sum_number=sum;
        }
        check_digit+=sum_number;
        if(multiplier%2==0){
            multiplier=1;
        }
        else{
            multiplier=2;
        }
    }
    check_digit=10-check_digit%10
    if(check_digit==last_digit){
        return true;
    }
    return false;
}

app.get('/', (req, res) => {
    res.render('page', {
      });
 });
 app.get('/action', (req, res) => {
    res.render('page',{
      });
 });

app.post('/action', (req, res) => {
    //console.log(req.body)
    stat="True";
    if(!checkExpiry(req.body.expiry)){
        stat="False"
    }
    if(!checkCVV(req.body.number,req.body.cvv)){
        stat="False"
    }
    if(!checkPAN(req.body.number)){
        stat="False"
    }
    if(!checkLUHN(req.body.number)){
        stat="False"
    }
    res.render('page', {
        status:stat,
    });
    //console.log(checkExpiry(req.body.expiry))
    //console.log(checkCVV(req.body.number,req.body.cvv))
    //console.log(checkPAN(req.body.number))
    //console.log(checkLUHN(req.body.number))
  });

app.listen(3000, () => {
    console.log("App listening on port 3000");
})
